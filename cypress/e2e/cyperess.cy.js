
describe("Testing Todo App", () => {
    beforeEach(() => {
      cy.visit("http://localhost:3000");
    });
  
    it("Add todos", () => {
      cy.contains("Click here to login").click();
      cy.get("#title").type("Faire les courses").type("{enter}");
      cy.get("#title").type("Ranger les courses").type("{enter}");
      cy.contains("Faire les courses");
      cy.contains("Ranger les courses");
    });
  
    it("Count todos", () => {
      cy.contains("Click here to login").click();
      cy.get("#title").type("RDV dentiste").type("{enter}");
      cy.get("#title").type("Sortir le chien").type("{enter}");
      cy.contains("Total Todos: 2");
    });
  
    it("Check todo", () => {
      cy.contains("Click here to login").click();
      cy.get("#title").type("Etre le meilleur dresseur").type("{enter}");
      cy.get('[type="checkbox"]').check();
      cy.contains("Selected Todos: 1");
    });
  });
  
  